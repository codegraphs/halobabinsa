import AsyncStorage from '@react-native-async-storage/async-storage'
import { Alert } from 'react-native'
import { refreshToken } from '../services/vendor/app'

export const refreshtoken = async () => {
    refreshToken().then( result => {
        saveToken(result.data.access_token)
    }).catch(error => {
        console.log(error)
    })
}

const saveToken = async (token) => {
    try {
        await AsyncStorage.setItem('token', token)
    } catch (error) {
        Alert.alert("Peringatan", error.toString())
    }
}