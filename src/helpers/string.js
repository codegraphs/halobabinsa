import { Title } from "native-base";

export function subTitle(title) {
    if (title.length <= 65) {
        return title;
    } else {
        return title.substring(0, 62) + '...';
    }
}

export function subAddress(title) {
    if (title.length <= 65) {
        return title;
    } else {
        return title.substring(0, 62) + '...';
    }
}