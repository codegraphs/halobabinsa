import React from 'react'
import { View, Text, TextInput } from 'react-native'

class Input extends React.Component {
    render() {
        return(
            <View style={{ margin: 5 }}>
                <Text style={{ fontWeight: 'bold'}}>{ this.props.title }</Text>
                <TextInput style={{ backgroundColor: '#fff', borderColor: '#f0f0f0', borderWidth: 1, paddingVertical: 8, paddingHorizontal: 8, marginVertical: 10, borderRadius: 5}} placeholder={ this.props.placeholder } {...this.props}/>
            </View>
        );
    }
}

export default Input;