import React from 'react'
import { View, Text, ActivityIndicator } from 'react-native'
import Modal from 'react-native-modal'

const Loading = (props) => {
    return(
        <View>
            <Modal isVisible={ props.visible } style={{ alignItems: 'center', justifyContent: 'center'}}>
                <View style={{ backgroundColor: '#fff', height: 150, width: 150, alignItems: 'center', justifyContent: 'center', borderRadius: 15 }}>
                    <ActivityIndicator size="large" color="#333" style={{ marginBottom: 30 }}/>
                    <Text>Mohon Menunggu</Text>
                </View>
            </Modal>
        </View>
    );
}

export default Loading