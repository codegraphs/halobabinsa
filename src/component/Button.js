import React from 'react'
import { Text, ActivityIndicator, View } from 'react-native'
import { Button } from 'native-base'

const Buttons = (props) => {
    if (props.disabled == true) {
        return(
            <Button block danger style={{ marginVertical: 15, borderRadius: 8 }}>
                <ActivityIndicator size="small" color="#0000ff" />
            </Button>
        )   
    } else {
        return(
            <Button block danger style={{ marginVertical: 15, borderRadius: 8 }} {...props}>
                <Text style={{ color: '#fff', fontWeight: 'bold', display: 'flex' }}>{ props.title }</Text>
            </Button>
        )
    }
}

export default Buttons