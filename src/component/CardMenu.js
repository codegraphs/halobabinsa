import React from 'react';
import { Text, Card, CardItem, View } from 'native-base';

class CardMenu extends React.Component {
    render() {
        return(
            <View style={{ justifyContent: 'center', alignItems: 'center'}}>
                <Card style={{ borderColor: 0, paddingHorizontal: 25 }} transparent >
                    <CardItem style={{ backgroundColor: 'grey', borderRadius: 10}}>
                        { this.props.children }
                    </CardItem>
                </Card>                    
                <Text style={{ fontWeight: 'bold', fontSize: 12 }}>{ this.props.title }</Text>
            </View>
        );
    }
}
export default CardMenu;