import React from 'react'
import { View, Text } from 'react-native'
import { Icon } from 'native-base'
import Modal from 'react-native-modal'
import Button from '../component/Button'

const ModalNotice = (props) => {
    return(
        <Modal isVisible={ props.isVisible } style={{ alignItems: 'center', justifyContent: 'center'}}>
            <View style={{ backgroundColor: '#fff', height: 170, width: 250, padding: 15, alignItems: 'center', justifyContent: 'center', borderRadius: 15 }}>
                {
                    (props.status == 'success') ?
                        <Icon name="check" type="FontAwesome" style={{ color: '#27AE60', fontSize: 50, marginBottom: 20}}/>
                    :
                        <Icon name="remove" type="FontAwesome" style={{ color: '#EB5757', fontSize: 50, marginBottom: 20}}/>
                }
                <Text>{ props.message }</Text>
                <Button disabled={ false } title="Oke" onPress={props.onClose}/>
            </View>
        </Modal>
    )
}

export default ModalNotice