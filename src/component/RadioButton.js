import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

class RadioButton extends React.Component {
    state = {
		value: null,
    };
    
    render() {
        const { options } = this.props;
        const { value } = this.state;
        
        return(
            <View style={{ flexDirection: 'row', marginVertical: 10}}>
				{options.map(item => {
					return (
						<View key={item.key} style={styles.buttonContainer}>
							<TouchableOpacity
								style={styles.circle}
								onPress={() => {
									this.setState({
										value: item.key,
									});
								}}
							>
								{value === item.key && <View style={styles.checkedCircle} />}
							</TouchableOpacity>
                            <Text>{item.text}</Text>
						</View>
					);
				})}
			</View>
        );
    }
}

const styles = StyleSheet.create({
	buttonContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
        marginRight: 30
	},

	circle: {
		height: 20,
		width: 20,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: '#ACACAC',
		alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10
	},
  
	checkedCircle: {
		width: 14,
		height: 14,
		borderRadius: 7,
		backgroundColor: '#794F9B',
	},
});


export default RadioButton;