import React from 'react';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { View } from 'react-native';

const LoadingListYt = ({ visible }) => {
    return(
        <View style={{ display: (visible == true) ? "flex" : "none" }}>
            <View style={{ backgroundColor:'#fff', width: '100%', marginVertical: 5 }}>
                <SkeletonPlaceholder backgroundColor="#fff" width='100%'>
                <View style={{ width: '100%', height: 200, marginBottom: 10 }}></View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <View style={{ width: 60, height: 60, borderRadius: 50 }} />
                    <View style={{ marginBottom: 20}}>
                    <View style={{ width: 250, height: 20, borderRadius: 4 }} />
                    <View style={{ marginTop: 6, width: 200, height: 20, borderRadius: 4 }} />
                    </View>
                </View>
                </SkeletonPlaceholder>
            </View>

            <View style={{ backgroundColor:'#fff', width: '100%', marginVertical: 5 }}>
                <SkeletonPlaceholder backgroundColor="#fff" width='100%'>
                <View style={{ width: '100%', height: 200, marginBottom: 10 }}></View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <View style={{ width: 60, height: 60, borderRadius: 50 }} />
                    <View style={{ marginBottom: 20}}>
                    <View style={{ width: 250, height: 20, borderRadius: 4 }} />
                    <View style={{ marginTop: 6, width: 200, height: 20, borderRadius: 4 }} />
                    </View>
                </View>
                </SkeletonPlaceholder>
            </View>

            <View style={{ backgroundColor:'#fff', width: '100%', marginVertical: 5 }}>
                <SkeletonPlaceholder backgroundColor="#fff" width='100%'>
                <View style={{ width: '100%', height: 200, marginBottom: 10 }}></View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <View style={{ width: 60, height: 60, borderRadius: 50 }} />
                    <View style={{ marginBottom: 20}}>
                    <View style={{ width: 250, height: 20, borderRadius: 4 }} />
                    <View style={{ marginTop: 6, width: 200, height: 20, borderRadius: 4 }} />
                    </View>
                </View>
                </SkeletonPlaceholder>
            </View>

            <View style={{ backgroundColor:'#fff', width: '100%', marginVertical: 5 }}>
                <SkeletonPlaceholder backgroundColor="#fff" width='100%'>
                <View style={{ width: '100%', height: 200, marginBottom: 10 }}></View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <View style={{ width: 60, height: 60, borderRadius: 50 }} />
                    <View style={{ marginBottom: 20}}>
                    <View style={{ width: 250, height: 20, borderRadius: 4 }} />
                    <View style={{ marginTop: 6, width: 200, height: 20, borderRadius: 4 }} />
                    </View>
                </View>
                </SkeletonPlaceholder>
            </View>

            <View style={{ backgroundColor:'#fff', width: '100%', marginVertical: 5 }}>
                <SkeletonPlaceholder backgroundColor="#fff" width='100%'>
                <View style={{ width: '100%', height: 200, marginBottom: 10 }}></View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <View style={{ width: 60, height: 60, borderRadius: 50 }} />
                    <View style={{ marginBottom: 20}}>
                    <View style={{ width: 250, height: 20, borderRadius: 4 }} />
                    <View style={{ marginTop: 6, width: 200, height: 20, borderRadius: 4 }} />
                    </View>
                </View>
                </SkeletonPlaceholder>
            </View>
        </View>
    );
}

export default LoadingListYt;