const ytApiUrl = 'https://www.googleapis.com/youtube/v3/';
const appApiUrl = 'http://162.0.224.91:3001/api/v1/auth/public/';
const baseApiUrl = 'http://162.0.224.91:3001/api/v1/';

export { 
    ytApiUrl,
    appApiUrl,
    baseApiUrl
};