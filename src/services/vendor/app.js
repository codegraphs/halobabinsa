import { appApiUrl, baseApiUrl } from '../config'
import axios from 'axios'

export function register(first_name, last_name, email, phone, password) {
    return new Promise((resolve, reject) => {
        const data = {
            first_name: first_name,
            last_name: last_name,
            email: email,
            phone: phone,
            password: password
        }

        axios.post(`${appApiUrl}` + 'signup', 
        JSON.stringify(data), {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            resolve(response)
        }).catch(error => {
            reject(error.response)
        })
    })
}

export function login(email, password) {
    return new Promise((resolve, reject) => {
        const data = {
            username: email,
            password: password
        }

        axios.post(`${appApiUrl}` + 'signin', 
            JSON.stringify(data), {
                headers: {
                    'Content-Type': 'application/json'
                }
        }).then(response => {
                resolve(response.data)
        }).catch(error => {
                reject(error)
        })
    });
}

export function laporan(data) {
    const formData = new FormData();
    formData.append(data)

    return new Promise((resolve, reject) => {
        axios.post(`${appApiUrl}` + 'report',
            JSON.stringify(formData), {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'x-access-token': `${token}`
                }
        }).then(response => {
            resolve(response.data)
        }).catch(error => {
            reject(error.response)
        })
    })
}

export function getProfile(token) {
    return new Promise((resolve, reject) => {
        axios.get(`${baseApiUrl}` + 'public/profile', {
            headers: {
                'x-access-token': token
            }
        }).then( result => {
            resolve(result.data)
        }).catch( error => {
            reject(error.response)
        })
    })
}

export function updateFcmToken(token, fcm) {
    return new Promise((resolve, reject) => {
        const data = {
            'fcm_token': fcm
        }

        axios.put(`${baseApiUrl}` + 'public/update-fcm-token', data, {
            headers: {
                'x-access-token': token
            }
        }).then(result => {
            resolve(result)
        }).catch(error => {
            reject(error.response)
        })
    })
}

export function updateProfile(token, data) {
    return new Promise((resolve, reject) => {
        axios.put(`${baseApiUrl}` + 'public/profile', data, {
            headers: {
                'x-access-token': token
            }
        }).then( result => {
            resolve(result.data)
        }).catch( error => {
            reject(error.response)
        })
    })
}

export function updatePassword(token, data) {
    return new Promise((resolve, reject) => {
        axios.put(`${baseApiUrl}` + 'public/update-password', data, {
            headers: {
                'x-access-token': token
            }
        }).then( result => {
            resolve(result.data)
        }).catch( error => {
            reject(error.response)
        })
    })
}

export function refreshToken(token) {
    return new Promise((resolve, reject) => {
        axios.post(`${appApiUrl}` + 'refresh-token', {
            headers: {
                'x-access-token': token
            }
        }).then(result => {
            resolve(result)
        }).catch(error => {
            reject(error)
        })
    })
}