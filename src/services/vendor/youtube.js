import { ytApiUrl } from '../config'
import axios from 'axios'

//config
const key = 'AIzaSyCK6mtOzzeFG4W7o1k7B0XuOJPxWjZaB2E';
const channelId = 'UCkGqMrkCgjHt72GUTXzoySA';

export function channel() {
    return new Promise((resolve, reject) => {
        axios.get(`${ytApiUrl}` + 'channels?part=snippet&id=' + `${channelId}` + '&key=' + `${key}`)
            .then(response => {
                resolve(response.data.items[0].snippet)
            })
            .catch(error => {
                reject(error)
            })
    });
}

export function getFiveVideos() {
    return new Promise((resolve, reject) => {
        axios.get(`${ytApiUrl}` + 'search?key=' + `${key}` + '&channelId=' + `${channelId}` + '&maxResults=5&order=date&part=snippet')
            .then(response => {
                resolve(response.data.items)
            })
            .catch(error => {
                reject(error)
            });
    });
}