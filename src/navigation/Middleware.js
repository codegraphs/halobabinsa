import React, { useEffect } from 'react';
import { View, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Splash = ({
    navigation
}) => {
    useEffect(() =>{
        setTimeout(() => {
            const auth = AsyncStorage.getItem("token").then( res => {
                if (res != null) {
                    navigation.navigate("MainAuth")
                } else {
                    navigation.navigate("Main")   
                }
            })
        }, 3000);
    })

    return(
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator color="#888"/>
        </View>
    );
}

export default Splash;