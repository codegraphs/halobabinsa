import React, { useEffect } from 'react';
import { View, ActivityIndicator } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage'

import { AuthContext } from './context'
/**
 * import Screen
 */
import RootStackScreen from '../screen/RootStackScreen'
import Main from '../screen/Main'
import YoutubePlayerWebView from '../screen/YoutubePlayerWebView'
import CameraPanic from '../screen/CameraPanic'
import VoicePanic from '../screen/VoicePanic'
import Report from '../screen/Report'
import EditProfile from '../screen/EditProfile'
import UpdatePassword from '../screen/UpdatePassword'
import Eula from '../screen/Eula'
import About from '../screen/About'

const Stack = createStackNavigator();

const Routes = () => {


  const initialLoginState = {
    isLoading: true,
    userName: null,
    userToken: null,
  }

  const loginReducer = (prevState, action) => {
    switch( action.type ) {
      case 'RETRIEVE_TOKEN': 
        return {
          ...prevState,
          userToken: action.token,
          isLoading: false,
        };
      case 'LOGIN': 
        return {
          ...prevState,
          userName: action.id,
          userToken: action.token,
          isLoading: false,
        };
      case 'LOGOUT': 
        return {
          ...prevState,
          userName: null,
          userToken: null,
          isLoading: false,
        };
      case 'REGISTER': 
        return {
          ...prevState,
          userName: action.id,
          userToken: action.token,
          isLoading: false,
        };
    }
  }

  const [loginState, dispatch] = React.useReducer(loginReducer, initialLoginState)

  const authContext = React.useMemo(() => ({
    signIn: async(id, token) => {
      try {
        await AsyncStorage.setItem('token', token)  
      } catch (error) {
        console.log(error)
      }

      dispatch({ type: 'LOGIN', id: id, token: token})
      console.log(loginState.userToken)
    },
    signOut: async() => {
      try {
        await AsyncStorage.removeItem('token')  
      } catch (error) {
        console.log(error)
      }

      dispatch({ type: 'LOGOUT' })
    },
    signUp: () => {
      setUserToken(null)
      setIsLoading(false)
    },
  }));

  useEffect(() => {
    setTimeout(async() => {
      let userToken = null;

      try {
        userToken = await AsyncStorage.getItem('token')
      } catch (error) {
        
      }
      dispatch({ type: 'RETRIEVE_TOKEN', token: userToken})
    }, 1000)
  }, [])

  if (loginState.isLoading) {
    return(
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="large" color="#888"/>
      </View>
    );
  }

  return (
    <AuthContext.Provider value={{
      signIn: async(id, token) => {
        try {
          await AsyncStorage.setItem('token', token)  
        } catch (error) {
          console.log(error)
        }
  
        dispatch({ type: 'LOGIN', id: id, token: token})
        console.log(loginState.userToken)
      },
      signOut: async() => {
        try {
          await AsyncStorage.removeItem('token')  
        } catch (error) {
          console.log(error)
        }
        console.log('LOGOUT')
        dispatch({ type: 'LOGOUT' })
      },
      signUp: () => {
        setUserToken(null)
        setIsLoading(false)
      },
    }}>
      <NavigationContainer>
        { loginState.userToken !== null ? (
          <Stack.Navigator screenOptions={{ headerShown: false}}>
            <Stack.Screen name="Main" component={Main} />
            <Stack.Screen name="YTWV" component={YoutubePlayerWebView} />
            <Stack.Screen name="CameraPanic" component={CameraPanic} />
            <Stack.Screen name="VoicePanic" component={VoicePanic} />
            <Stack.Screen name="Report" component={Report} />
            <Stack.Screen name="EditProfile" component={EditProfile}/>
            <Stack.Screen name="UpdatePassword" component={UpdatePassword}/>
            <Stack.Screen name="Eula" component={Eula}/>
            <Stack.Screen name="About" component={About}/>
          </Stack.Navigator>
        ) : (
          <RootStackScreen/>
        )

        }
      </NavigationContainer>
    </AuthContext.Provider>
  )
}

export default Routes;