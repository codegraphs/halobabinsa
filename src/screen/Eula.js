import React from 'react'
import { View, Text, TouchableOpacity, ScrollView } from 'react-native'
import { Icon } from 'native-base'

class Eula extends React.Component {
    render() {
        return(
            <View style={{ flex: 1 }}>
                <View style={{ height: 45, backgroundColor: '#fff', flexDirection: 'row'}}>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={ () => this.props.navigation.goBack() }>
                            <Icon type="Feather" name="arrow-left" style={{ color: "#777", fontWeight: 300}}/>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '80%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{ fontWeight: 'bold', color: '#777'}}>Syarat dan Ketentuan</Text>
                    </View>
                    <View style={{ width: '10%'}}> 
                    </View>
                </View>
                <ScrollView style={{ padding: 15, backgroundColor: '#fff'}}>
                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>Syarat & Ketentuan</Text>

                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>Selamat Datang di HaloBabinsa</Text>

                    <Text style={{ marginVertical: 15 }}>
                        Syarat & ketentuan yang ditetapkan di bawah ini mengatur Pengguna layanan yang yang disediakan oleh HaloBabinsa, baik berupa informasi, teks, grafik, atau data lain, unduhan, unggahan, atau menggunakan layanan (secara garis besar disebut sebagai data atau konten). Pengguna disarankan membaca dengan seksama karena dapat berdampak kepada hak dan kewajiban Pengguna di bawah hukum.
                        Dengan mendaftar dan/atau menggunakan aplikasi HaloBabinsa, maka Pengguna dianggap telah membaca, mengerti, memahami, dan menyetujui semua isi dalam Syarat & Ketentuan. Penggunaan dan akses Pengguna ke layanan memerlukan akun yang terhubung dengan akun Google dikondisikan pada persetujuan dan kepatuhan Pengguna dengan syarat-syarat yang berlaku. Syarat & Ketentuan ini merupakan bentuk kesepakatan yang dituangkan dalam sebuah perjanjian yang sah antara Pengguna dengan aplikasi HaloBabinsa. Jika Pengguna tidak menyetujui salah satu, sebagian, atau seluruh isi Syarat & Ketentuan, maka Pengguna tidak diperkenankan menggunakan layanan di aplikasi HaloBabinsa.
                        Perjanjian ini berlaku sejak tanggal 22 November 2020
                        Kami berhak untuk mengubah Syarat & Ketentuan ini dari waktu ke waktu tanpa pemberitahuan. Pengguna mengakui dan menyetujui bahwa merupakan tanggung jawab Pengguna untuk meninjau Syarat & Ketentuan ini secara berkala untuk mengetahui perubahan apa pun. Dengan tetap mengakses dan menggunakan layanan HaloBabinsa , maka Pengguna dianggap menyetujui perubahan-perubahan dalam Syarat & Ketentuan.
                        Larangan
                        Pengguna dapat menggunakan layanan HaloBabinsa hanya untuk tujuan yang sah. Pengguna tidak dapat menggunakan layanan HaloBabinsa dengan cara apa pun yang:
                        melanggar hukum atau peraturan lokal, nasional maupun internasional yang berlaku;
                        dengan cara apa pun, melanggar hukum atau melakukan penipuan, atau memiliki tujuan atau efek yang melanggar hukum atau menipu;
                        merugikan atau berupaya untuk menyakiti individu atau entitas lain dengan cara apa pun;
                        memperbanyak, menggandakan, menyalin, menjual, menjual kembali, atau mengeksploitasi bagian manapun dari layanan, penggunaan layanan, atau akses ke layanan tanpa izin tertulis dari HaloBabinsa ;
                        atau akan menyalahgunakan baik secara verbal, fisik, tertulis, atau penyalahgunaan lainnya (termasuk ancaman kekerasan atau retribusi) dari setiap pengguna, karyawan, anggota, atau petugas HaloBabinsa;
                        dan hal-hal yang telah disebutkan di atas akan berakibat pada pemutusan akun secepatnya dan akan diproses ke ranah hukum.
                        Pengguna wajib menggunakan layanan HaloBabinsa dengan bijak dan baik yaitu dengan menghindari kata-kata seperti:
                        hinaan dan/atau kata kasar;
                        mengandung SARA;
                        bersifat pornografi; dan
                        mengundang provokasi
                        dan hal-hal yang telah disebutkan di atas akan berakibat pada penghapusan konten yang dipublikasikan di HaloBabinsa.
                        Pengguna menyatakan dan menjamin bahwa tidak akan menggunakan layanan untuk mengunggah, memasang ( posting ), memberikan tautan, email , mengirimkan ( transmit ), atau menyediakan bahan apa pun yang mengandung virus atau kode komputer lainnya, file , atau program yang dirancang untuk mengganggu, merusak, atau membatasi fungsi perangkat lunak komputer atau perangkat keras atau peralatan telekomunikasi lainnya. Pengguna juga tidak akan mengunggah atau mendistribusikan program komputer yang merusak, mengganggu, memotong, atau mengambil alih sistem, data, atau informasi pribadi. Pengguna juga menyatakan dan menjamin bahwa Pengguna tidak akan mengganggu fungsi situs dengan cara apa pun.
                    </Text>

                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>Privasi</Text>

                    <Text style={{ marginVertical: 15 }}>Privasi Pengguna adalah hal yang sangat penting bagi kami. Pengguna layanan tunduk pada kebijakan privasi HaloBabinsa , yang menjelaskan bagaimana HaloBabinsa mengumpulkan dan menggunakan informasi pribadi Pengguna.</Text>

                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>Retensi Data</Text>

                    <Text style={{ marginVertical: 15 }}>HaloBabinsa akan menyimpan data Pengguna (data atau informasi yang Pengguna berikan kepada HaloBabinsa atau yang HaloBabinsa kumpulkan dari Pengguna, termasuk data pribadi Pengguna, baik pada saat Pengguna melakukan pendaftaran di aplikasi, mengakses aplikasi, maupun mempergunakan layanan-layanan pada aplikasi (selanjutnya disebut sebagai data)) di server database utama selama akun Pengguna saat ini dan aktif. Jika Pengguna menghapus data Pengguna, sistem akan segera menghapus data Pengguna secara permanen dari server database utama. HaloBabinsa tidak bertanggung jawab atas kerugian atau kerusakan dari draf data yang Pengguna simpan di perangkat komputasi Pengguna dan data yang disimpan di server database utama kami yang terjadi sebagai akibat dari penggunaan layanan kami yang tidak semestinya. Dan kami tidak akan mengakomodasi permintaan untuk memulihkan data dari sistem cadangan kami kecuali ada kehilangan data karena kegagalan sistem atau perangkat keras di pihak kami. Tidak ada pengecualian.</Text>

                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>Penghentian Layanan</Text>

                    <Text style={{ marginVertical: 15 }}>Pengguna setuju bahwa kami dapat, atas kebijakan kami sendiri, menangguhkan atau menghentikan akses Pengguna ke semua atau sebagian dari layanan dan sumber daya kami dengan atau tanpa pemberitahuan dan karena alasan apa pun, termasuk, tanpa batasan, pelanggaran Syarat & Ketentuan ini. Setiap aktivitas yang dicurigai ilegal, curang (penipuan), atau tidak wajar dapat menjadi alasan untuk mengakhiri hubungan Pengguna dan dapat dirujuk ke otoritas penegak hukum yang sesuai. Setelah penangguhan atau penghentian, hak Pengguna untuk menggunakan sumber daya yang kami sediakan akan segera berhenti, dan kami berhak untuk menghapus informasi apa pun yang Pengguna miliki di file kami, termasuk akun atau informasi saat login .</Text>

                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>Kepemilikan Data</Text>

                    <Text style={{ marginVertical: 15 }}>Pengguna sebagai pengguna layanan HaloBabinsa memiliki tanggung jawab untuk memastikan bahwa Pengguna memiliki hak atau izin yang diperlukan untuk konten Pengguna.</Text>

                    <Text style={{ marginVertical: 15 }}>
                        Pengguna mempertahankan kepemilikan konten Pengguna. Kami tidak mengklaim hak kekayaan intelektual atas materi yang Pengguna berikan kepada layanan. Profil Pengguna, data, dan materi lain yang diunggah akan tetap menjadi milik Pengguna.
                        HaloBabinsa tidak menyaring atau memantau konten, tetapi HaloBabinsa dan yang ditunjuk memiliki hak (tetapi bukan kewajiban) atas kebijakannya sendiri untuk menolak atau menghapus konten apa pun yang tersedia melalui layanan.
                    </Text>

                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>Modifikasi Layanan</Text>

                    <Text style={{ marginVertical: 15 }}>
                        HaloBabinsa berhak setiap saat dan dari waktu ke waktu untuk memodifikasi atau menghentikan, sementara atau secara permanen, layanan (atau bagiannya) dengan atau tanpa pemberitahuan.
                        HaloBabinsa tidak akan bertanggung jawab kepada Pengguna atau kepada pihak ketiga untuk modifikasi apa pun, penangguhan, atau penghentian layanan.
                    </Text>

                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>Penolakan Jaminan</Text>

                    <Text style={{ marginVertical: 15 }}>Layanan HaloBabinsa dan semua elemennya disediakan atas dasar sebagaimana adanya tanpa jaminan apa pun, tersurat maupun tersirat. HaloBabinsa tidak membuat jaminan atau pernyataan mengenai keakuratan konten yang ada di layanan. HaloBabinsa tidak menjamin bahwa layanan akan beroperasi atau bekerja tanpa terganggu atau bebas dari kesalahan, atau bahwa server basis data utama atau server host akan bebas dari virus atau kode berbahaya atau kesalahan tak terduga lainnya. HaloBabinsa tidak membuat jaminan bahwa informasi yang disajikan pada layanan merupakan informasi terkini atau akurat. Beberapa negara dan wilayah hukum tidak mengizinkan pembatasan pada jaminan, sehingga pembatasan di atas mungkin tidak berlaku bagi Pengguna. Hal tersebut di atas harus dilaksanakan hingga batas maksimal yang diizinkan oleh hukum yang berlaku.</Text>

                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>Batasan Tanggung Jawab</Text>

                    <Text style={{ marginVertical: 15 }}>
                        Pengguna memahami dan menyetujui bahwa HaloBabinsa tidak akan bertanggung jawab atas kerusakan langsung, tidak langsung, insidental, khusus, konsekuensial termasuk tetapi tidak terbatas, pada kerusakan untuk kehilangan keuntungan, penggunaan, data, atau kerugian tidak berwujud lainnya (bahkan jika HaloBabinsa telah diberitahu tentang kemungkinan kerusakan tersebut), yang dihasilkan dari:
                        penggunaan atau ketidakmampuan untuk menggunakan layanan;
                        akses tidak sah atau perubahan transmisi atau data Pengguna;
                        pernyataan atau perilaku pihak ketiga di layanan HaloBabinsa ;
                        atau hal lain yang berkaitan dengan layanan.
                    </Text>

                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>Kontak Informasi</Text>

                    <Text style={{ marginVertical: 15 }}>Jika Pengguna memiliki pertanyaan, komentar atau masalah tentang Syarat & Ketentuan ini, silakan berkirim email ke dev@aditara.id</Text>

                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>Kebijakan Privasi</Text>

                    <Text style={{ marginVertical: 15 }}>
                        Kebijakan Privasi ini adalah komitmen nyata dari HaloBabinsa untuk menghargai dan melindungi setiap data atau informasi pribadi Pengguna aplikasi HaloBabinsa .
                        Kebijakan Privasi ini (beserta syarat-syarat penggunaan sebagaimana tercantum dalam "Syarat & Ketentuan" dan informasi lain yang tercantum di aplikasi HaloBabinsa) menetapkan dasar atas perolehan, pengumpulan, pengolahan, penganalisisan, penampilan, pembukaan, dan/atau segala bentuk pengelolaan yang terkait dengan data atau informasi yang Pengguna berikan kepada HaloBabinsa atau yang HaloBabinsa kumpulkan dari Pengguna, termasuk data pribadi Pengguna, baik pada saat Pengguna melakukan pendaftaran di aplikasi, mengakses aplikasi, maupun mempergunakan layanan-layanan pada aplikasi (selanjutnya disebut sebagai "data").
                        Dengan mengakses dan/atau mempergunakan layanan HaloBabinsa, Pengguna menyatakan bahwa setiap data Pengguna merupakan data yang benar dan sah, serta Pengguna dianggap telah membaca dan memahami dan memberikan persetujuan kepada HaloBabinsa untuk memperoleh, mengumpulkan, menyimpan, mengelola, dan mempergunakan data tersebut sebagaimana tercantum dalam Kebijakan Privasi maupun Syarat & Ketentuan HaloBabinsa.
                        Perolehan dan Pengumpulan Data Pengguna
                        HaloBabinsa mengumpulkan data Pengguna dengan tujuan untuk mengelola dan memperlancar proses penggunaan situs, serta tujuan-tujuan lainnya selama diizinkan oleh peraturan perundang-undangan yang berlaku. Adapun data Pengguna yang dikumpulkan adalah sebagai berikut:
                        Data yang diserahkan secara mandiri oleh Pengguna, termasuk namun tidak terbatas, pada data yang diserahkan pada saat Pengguna:
                        Membuat atau memperbarui akun HaloBabinsa, termasuk di antaranya nama pengguna (username), Nomor NIK, alamat email, nomor telepon, password, alamat sesuai dengan KTP, foto, dan lain-lain yang diminta HaloBabinsa;
                        mengisi survei yang dikirimkan oleh HaloBabinsa atau atas nama HaloBabinsa;
                        melakukan interaksi dengan Pengguna lainnya melalui fitur pesan, diskusi, ulasan, rating, dan sebagainya; dan
                        menggunakan fitur yang membutuhkan izin akses terhadap perangkat Pengguna.
                    </Text>

                    <Text style={{ marginVertical: 15 }}>Data yang terekam pada saat Pengguna mempergunakan aplikasi HaloBabinsa, termasuk namun tidak terbatas pada:</Text>

                    <Text style={{ marginVertical: 15 }}>- Data lokasi riil atau perkiraannya seperti alamat IP, lokasi Wi-Fi, geolocation, dan sebagainya;</Text>
                    <Text style={{ marginVertical: 15 }}>- Data berupa waktu dari setiap aktivitas Pengguna, termasuk aktivitas pendaftaran, login, dan lain sebagainya;</Text>
                    <Text style={{ marginVertical: 15 }}>- Data seluruh konten yang Pengguna buat, Pengguna bagikan, atau Pengguna kirimkan dalam bentuk audio, video, teks, gambar, atau jenis media atau file perangkat lunak lainnya disediakan atas nama Pengguna, termasuk informasi di konten atau tentang konten yang Pengguna sediakan, seperti lokasi foto atau tanggal pembuatan file;</Text>
                    <Text style={{ marginVertical: 15 }}>- Data terkait topik yang dibaca dan umpan-balik;</Text>
                    <Text style={{ marginVertical: 15 }}>- Data yang diberikan oleh orang lain tentang Pengguna saat menggunakan layanan, termasuk saat mereka mengirim pesan kepada Pengguna atau mengunggah informasi tentang Pengguna;</Text>
                    <Text style={{ marginVertical: 15 }}>- Data semua komunikasi dengan pengguna lainnya;</Text>
                    <Text style={{ marginVertical: 15 }}>- Data penggunaan atau preferensi Pengguna, di antaranya interaksi Pengguna dalam menggunakan aplikasi, pilihan yang disimpan, serta pengaturan yang dipilih. Data tersebut diperoleh menggunakan cookies, pixel tags, dan teknologi serupa yang menciptakan dan mempertahankan pengenal unik;</Text>
                    <Text style={{ marginVertical: 15 }}>
                        - Data perangkat, di antaranya jenis perangkat yang digunakan untuk mengakses aplikasi, termasuk model perangkat keras, sistem operasi dan versinya, perangkat lunak, nama file dan versinya, pilihan bahasa, pengenal perangkat unik, pengenal iklan, nomor seri, informasi gerakan perangkat;
                        dan/atau informasi jaringan seluler;
                    </Text>
                    <Text style={{ marginVertical: 15 }}>- Data catatan (log), di antaranya catatan pada server yang menerima data seperti alamat IP perangkat, tanggal dan waktu akses, fitur aplikasi atau laman yang dilihat, proses kerja aplikasi dan aktivitas sistem lainnya, jenis peramban, dan/atau situs atau layanan Pihak Ketiga yang Pengguna gunakan sebelum berinteraksi dengan aplikasi;</Text>
                    <Text style={{ marginVertical: 15 }}>- Data komunikasi, masukan, saran, dan ide pengguna yang dikirimkan kepada kami;</Text>
                    <Text style={{ marginVertical: 15 }}>- Data yang Pengguna berikan saat Pengguna atau organisasi Pengguna yang menghubungi atau berinteraksi dengan kami untuk dukungan terkait layanan;</Text>
                    <Text style={{ marginVertical: 15 }}>- Data yang diperoleh dari sumber lain, termasuk:</Text>
                    <Text>* Nama</Text>
                    <Text>* Alamat surat elektronik</Text>
                    <Text>* Jenis kelamin</Text>
                    <Text>* Foto</Text>
                    <Text>* User ID</Text>

                    <Text style={{ marginVertical: 15 }}>HaloBabinsa dapat menggabungkan data yang diperoleh dari sumber tersebut dengan data lain yang dimilikinya.</Text>

                    <Text style={{ marginVertical: 15 }}>Penggunaan Data</Text>

                    <Text style={{ marginVertical: 15 }}>
                        HaloBabinsa dapat menggunakan keseluruhan atau sebagian data yang diperoleh dan dikumpulkan dari Pengguna sebagaimana disebutkan dalam bagian sebelumnya untuk hal-hal sebagai berikut:
                        Memproses segala bentuk permintaan maupun aktivitas yang dilakukan oleh Pengguna melalui aplikasi.
                        Penyediaan fitur-fitur untuk memberikan, mewujudkan, memelihara, dan memperbaiki layanan kami, termasuk:
                        menawarkan, memperoleh, menyediakan, atau memfasilitasi layanan dan/atau produk-produk lainnya baik bersumber dari HaloBabinsa atau Pihak Ketiga;
                        memungkinkan fitur untuk memprivasikan akun HaloBabinsa Pengguna, seperti Like, Share, Bookmark; dan/atau
                        melakukan kegiatan internal yang diperlukan untuk menyediakan layanan pada situs/aplikasi HaloBabinsa, seperti pemecahan masalah software, bug, permasalahan operasional, melakukan analisis data, pengujian dan penelitian, dan untuk memantau dan menganalisis kecenderungan penggunaan dan aktivitas.
                        Menghubungi Pengguna melalui email, surat, telepon, fax, dan lain-lain, termasuk namun tidak terbatas, untuk membantu dan/atau memproses penyelesaian kendala.
                        Menggunakan informasi yang diperoleh dari Pengguna untuk tujuan penelitian, analisis, pengembangan dan pengujian guna meningkatkan keamanan dan keamanan layanan-layanan pada aplikasi, serta mengembangkan fitur pada aplikasi.
                        Menginformasikan kepada Pengguna terkait layanan, studi, survei, berita, perkembangan terbaru, acara dan lain-lain, baik melalui aplikasi maupun melalui media lainnya. HaloBabinsa juga dapat menggunakan informasi tersebut untuk menyajikan konten yang relevan tentang layanan HaloBabinsa.
                        Melakukan monitoring atau pun investigasi terhadap konten atau interaksi mencurigakan yang terindikasi mengandung unsur pelanggaran terhadap Syarat & Ketentuan atau ketentuan hukum yang berlaku, serta melakukan tindakan-tindakan yang diperlukan sebagai tindak lanjut dari hasil monitoring atau investigasi transaksi tersebut.
                        Dalam keadaan tertentu, HaloBabinsa mungkin perlu untuk menggunakan atau pun mengungkapkan data Pengguna untuk tujuan penegakan hukum atau untuk pemenuhan persyaratan hukum dan peraturan yang berlaku, termasuk dalam hal terjadinya sengketa atau proses hukum antara Pengguna dan HaloBabinsa.
                    </Text>

                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>Pengungkapan Data Pribadi Pengguna</Text>

                    <Text style={{ marginVertical: 15 }}>
                        HaloBabinsa menjamin tidak ada penjualan, pengalihan, distribusi atau meminjamkan data pribadi Pengguna kepada Pihak Ketiga lain, tanpa terdapat izin dari Pengguna, kecuali dalam hal-hal sebagai berikut:
                        Dibutuhkan adanya pengungkapan data Pengguna kepada mitra atau Pihak Ketiga lain yang membantu HaloBabinsa dalam menyajikan layanan pada aplikasi dan memproses segala bentuk aktivitas Pengguna dalam aplikasi, termasuk memproses transaksi, verifikasi pembayaran, pengiriman produk, dan lain-lain.
                        HaloBabinsa dapat menyediakan informasi yang relevan kepada Pihak Ketiga sesuai dengan persetujuan Pengguna untuk menggunakan layanan Pihak Ketiga, termasuk di antaranya aplikasi atau situs lain yang telah saling mengintegrasikan API (Application Programming Interface) atau layanannya.
                        Dibutuhkan adanya komunikasi antara Pihak Ketiga dengan Pengguna dalam hal penyelesaian kendala maupun hal-hal lainnya.
                        HaloBabinsa dapat menyediakan informasi yang relevan kepada vendor, konsultan, firma riset, atau penyedia layanan sejenis.
                        Pengguna menghubungi HaloBabinsa melalui media publik seperti blog, media sosial, dan fitur tertentu pada aplikasi, komunikasi antara Pengguna dan HaloBabinsa mungkin dapat dilihat secara publik.
                        HaloBabinsa dapat membagikan informasi Pengguna kepada anak perusahaan dan afiliasinya untuk membantu memberikan layanan atau melakukan pengolahan data untuk dan atas nama HaloBabinsa.
                        HaloBabinsa mengungkapkan data Pengguna dalam upaya mematuhi kewajiban hukum dan/atau adanya permintaan yang sah dari aparat penegak hukum.
                    </Text>

                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>Cookies</Text>

                    <Text style={{ marginVertical: 15 }}>
                        Cookies adalah file kecil yang secara otomatis akan mengambil tempat di dalam perangkat Pengguna yang menjalankan fungsi dalam menyimpan preferensi maupun konfigurasi Pengguna selama mengunjungi suatu situs.
                        Cookies tersebut tidak diperuntukkan untuk digunakan pada saat melakukan akses data lain yang Pengguna miliki di perangkat komputer Pengguna, selain dari yang telah Pengguna setujui untuk disampaikan.
                        Walaupun secara otomatis perangkat komputer Pengguna akan menerima cookies, Pengguna dapat menentukan pilihan untuk melakukan modifikasi melalui pengaturan browser Pengguna yaitu dengan memilih untuk menolak cookies (pilihan ini dapat membatasi layanan optimal pada saat melakukan akses ke situs).
                        HaloBabinsa menggunakan fitur Google Analytics Demographics and Interest. Data yang kami peroleh dari fitur tersebut, seperti umur, jenis kelamin, minat Pengguna, dan lain-lain, akan kami gunakan untuk pengembangan situs dan konten HaloBabinsa. Jika tidak ingin data Pengguna terlacak oleh Google Analytics, Pengguna dapat menggunakan Add-On Google Analytics Opt-Out Browser.
                        HaloBabinsa dapat menggunakan fitur-fitur yang disediakan oleh Pihak Ketiga dalam rangka meningkatkan layanan dan konten HaloBabinsa, termasuk di antaranya ialah penyesuaian dan penyajian iklan kepada setiap Pengguna berdasarkan minat atau riwayat kunjungan. Jika Pengguna tidak ingin iklan ditampilkan berdasarkan penyesuaian tersebut, maka Pengguna dapat mengaturnya melalui browser.
                    </Text>

                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>
                        Pilihan Pengguna dan Transparansi
                    </Text>

                    <Text style={{ marginVertical: 15 }}>
                        Perangkat seluler pada umumnya (android dan sebagainya) memiliki pengaturan sehingga aplikasi HaloBabinsa tidak dapat mengakses data tertentu tanpa persetujuan dari Pengguna. Perangkat Android akan memberikan pemberitahuan kepada Pengguna saat aplikasi HaloBabinsa pertama kali dimuat. Dengan menggunakan aplikasi dan memberikan akses terhadap aplikasi, Pengguna dianggap memberikan persetujuannya terhadap Kebijakan Privasi.
                        Setelah konten dibuat, Pengguna memiliki kesempatan untuk memberikan penilaian dan ulasan terhadap akun satu sama lain. Informasi ini mungkin dapat dilihat secara publik dengan persetujuan Pengguna.
                        Pengguna dapat mengakses dan mengubah informasi berupa alamat email, nomor telepon, tanggal lahir, jenis kelamin, daftar alamat, melalui fitur Pengaturan pada aplikasi.
                        Pengguna dapat menarik diri dari informasi atau notifikasi berupa buletin, ulasan, diskusi konten, pesan pribadi, atau pesan pribadi dari Admin yang dikirimkan oleh HaloBabinsa melalui fitur Pengaturan pada aplikasi. HaloBabinsa tetap dapat mengirimkan pesan atau email berupa keterangan transaksi atau informasi terkait akun Pengguna.
                        Sejauh diizinkan oleh ketentuan yang berlaku, Pengguna dapat menghubungi HaloBabinsa untuk melakukan penarikan persetujuan terhadap perolehan, pengumpulan, penyimpanan, pengelolaan, dan penggunaan data Pengguna. Apabila terjadi demikian maka Pengguna memahami konsekuensi bahwa Pengguna tidak dapat menggunakan layanan aplikasi HaloBabinsa.
                        Penyimpanan dan Penghapusan Informasi
                        HaloBabinsa akan menyimpan informasi selama akun Pengguna tetap aktif dan dapat melakukan penghapusan sesuai dengan ketentuan peraturan hukum yang berlaku.
                    </Text>

                    <Text style={{ marginVertical: 15, fontWeight: 'bold' }}>
                        Pembaruan Kebijakan Privasi
                    </Text>

                    <Text style={{ marginVertical: 15 }}>HaloBabinsa dapat sewaktu-waktu melakukan perubahan atau pembaruan terhadap Kebijakan Privasi ini. HaloBabinsa menyarankan agar Pengguna membaca secara seksama dan memeriksa halaman Kebijakan Privasi ini dari waktu ke waktu untuk mengetahui perubahan apa pun. Dengan tetap mengakses dan menggunakan layanan aplikasi HaloBabinsa, maka Pengguna dianggap menyetujui perubahan-perubahan dalam Kebijakan Privasi.</Text>
                </ScrollView>
            </View>
        );
    }
}

export default Eula