import React from 'react'
import { View, Text, TouchableOpacity, Image, PermissionsAndroid } from 'react-native'
import { Icon } from 'native-base'
import { SwipeablePanel } from 'rn-swipeable-panel';
import AudioRecorderPlayer, {
    AVEncoderAudioQualityIOSType,
    AVEncodingOption,
    AudioEncoderAndroidType,
    AudioSet,
    AudioSourceAndroidType,
} from 'react-native-audio-recorder-player';
import AsyncStorage from '@react-native-async-storage/async-storage';
import GetLocation from 'react-native-get-location'
import Geocoder from 'react-native-geocoding';
import Buttons from '../component/Button'
import ModalNotice from '../component/ModalNotice'
import axios from 'axios'

class VoicePanic extends React.Component {

    constructor() {
        super()
        this.state = {
            isRecord: false,
            recordSecs: 0,
            recordTime: '00:00:00',
            currentPositionSec: 0,
            currentDurationSec: 0,
            playTime: '00:00:00',
            duration: '00:00:00',
            afterRecord: false,
            latitude: null,
            longitude: null,
            address: '',
            token: '',
            audio: '',
            ready: true,
            onUpload: false,
            message: '',
            afterUpload: false,
            status: ''
        }

        this.audioRecorderPlayer = new AudioRecorderPlayer();
        this.audioRecorderPlayer.setSubscriptionDuration(0.09);
    }

    componentDidMount() {
        this.position()
        this.getToken()
    }

    async getToken() {
        try {
            const token = await AsyncStorage.getItem("token")
            this.setState({ token: token})
        } catch (error) {
            
        }
    }

    laporan() {
        this.setState({ onUpload: true })
        var formData = new FormData();

        formData.append('report_type', '5fa70a5cc1e5c7c1b8014857');
        formData.append('address', this.state.address);
        formData.append('latitude', this.state.latitude);
        formData.append('longitude', this.state.longitude);
        formData.append('description', 'Dari Panic Suara');
        formData.append('attachment', {
            uri: this.state.audio,
            type: 'video/mp4',
            name: 'laporan.mp4'
        });

        axios.post('http://162.0.224.91:3001/api/v1/report/',
            formData, {
                headers: {
                    'Content-Type': 'multipart/form-data; boundary=<calculated when request is sent>',
                    'x-access-token': `${this.state.token}`
                }
        }).then(response => {
            console.log(response.data)
            this.setState({ onUpload: false, afterCapture: false, afterUpload: true, message: "Berhasil Melapor Ke Babinsa", status: "success", afterRecord: false })
        }).catch(error => {
            console.log(error.response)
            this.setState({ onUpload: false, afterCapture: false, afterUpload: true, message: "Gagal Melapor Ke Babinsa", status: "fail", afterRecord: false })
        })
    }

    position() {
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        })
        .then(location => {
            this.setState({
                latitude: location.latitude,
                longitude: location.longitude
            });

            Geocoder.init('AIzaSyCK6mtOzzeFG4W7o1k7B0XuOJPxWjZaB2E');
            Geocoder.from({
                latitude: location.latitude,
                longitude: location.longitude
            }).then(result =>  {
                this.setState({
                    address: result.results[0].formatted_address,
                    ready: false
                })
            })
        })
        .catch(error => {
            const { code, message } = error;
            console.warn(code, message);
        })
    }

    onStartRecord = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'Permissions for write access',
                        message: 'Give permission to your storage to write a file',
                        buttonPositive: 'ok',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log('You can use the storage');
                } else {
                    console.log('permission denied');
                    return;
                }
            } catch (err) {
                console.warn(err);
                return;
            }
        }

        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
                    {
                        title: 'Permissions for write access',
                        message: 'Give permission to your storage to write a file',
                        buttonPositive: 'ok',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log('You can use the camera');
                } else {
                    console.log('permission denied');
                    return;
                }
            } catch (err) {
                console.warn(err);
                return;
            }
        }

        const result = await this.audioRecorderPlayer.startRecorder();
        this.audioRecorderPlayer.addRecordBackListener((e) => {
            this.setState({
                isRecord: true,
                recordSecs: e.current_position,
                recordTime: this.audioRecorderPlayer.mmssss(
                    Math.floor(e.current_position),
                ),
            });
            return;
        });
        console.log(result);
    };

    onStopRecord = async () => {
        const result = await this.audioRecorderPlayer.stopRecorder();
        this.audioRecorderPlayer.removeRecordBackListener();
        this.setState({
            recordSecs: 0,
            isRecord: false,
            afterRecord: true,
            audio: result
        });
        
        console.log(result);
    };

    onStartPlay = async () => {
        console.log('onStartPlay');
        const msg = await this.audioRecorderPlayer.startPlayer();
        console.log(msg);

        this.audioRecorderPlayer.addPlayBackListener((e) => {
            if (e.current_position === e.duration) {
                console.log('finished');
                // this.audioRecorderPlayer.stopPlayer();
            }

            this.setState({
                currentPositionSec: e.current_position,
                currentDurationSec: e.duration,
                playTime: this.audioRecorderPlayer.mmssss(Math.floor(e.current_position)),
                duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration)),
            });

            return;
        });
    };

    onPausePlay = async () => {
        await this.audioRecorderPlayer.pausePlayer();
    };

    onStopPlay = async () => {
        console.log('onStopPlay');
        this.audioRecorderPlayer.stopPlayer();
        this.audioRecorderPlayer.removePlayBackListener();
    };

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#d4d3d4' }}>
                <View style={{ height: '7%', flexDirection: 'row', backgroundColor: '#f1f1f1' }}>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Main')}>
                            <Icon type="Feather" name="arrow-left" style={{ color: "#333", fontWeight: 300 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '80%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Icon type="Feather" name="map-pin" style={{ fontSize: 12, marginRight: 5 }} /><Text style={{ fontSize: 12 }}>{ this.state.address.substring(0, 35) }</Text>
                    </View>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>

                    </View>
                </View>
                <View style={{ height: '93%', alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{ fontSize: 25, marginVertical: 15}}>{ this.state.recordTime }</Text>
                    <TouchableOpacity style={{ backgroundColor: 'red', height: 200, width: 200, alignItems: "center", justifyContent: "center", borderRadius: 100}} onPress={ () => (this.state.isRecord == false) ? this.onStartRecord() : this.onStopRecord()}>
                        {
                            (this.state.isRecord == false) ?
                            <Icon type="Feather" name="mic" style={{ color: '#fff', fontSize: 50 }}/>
                            :
                            <Icon type="Feather" name="square" style={{ color: '#fff', fontSize: 50 }}/>
                        }
                    </TouchableOpacity>
                </View>

                <SwipeablePanel isActive={this.state.afterRecord} fullWidth={true} openLarge={true} onClose={ () => this.setState({ afterRecord: false }) }>
                    <View style={{ flex: 1, padding: 10 }}>
                        <View style={{ flexDirection: 'row', marginBottom: 15, alignItems: 'center', justifyContent: 'center' }}>
                            <Icon type="Feather" name="map-pin" style={{ fontSize: 12, marginRight: 5 }}/><Text style={{ fontSize: 12 }}>{ this.state.address.substring(0, 35) }</Text>
                        </View>
                        <View style={{ height: 400, justifyContent: "center", alignItems: "center" }}>
                            <View>
                                <Text>{ this.state.playTime }</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginVertical: 10 }}>
                                <TouchableOpacity style={{ backgroundColor: 'red', height: 100, width: 100, alignItems: "center", justifyContent: "center", borderRadius: 50, margin: 10}} onPress={ () => this.onStartPlay() }>
                                    <Icon type="FontAwesome" name="play" style={{ color: '#fff', fontSize: 25 }}/>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ backgroundColor: 'red', height: 100, width: 100, alignItems: "center", justifyContent: "center", borderRadius: 50, margin: 10}} onPress={ () => this.onStopPlay() }>
                                    <Icon type="FontAwesome" name="stop" style={{ color: '#fff', fontSize: 25 }}/>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ justifyContent: 'flex-end', flex:1, alignItems: 'center'}}>
                            <Buttons title="Kirim" disabled={ this.state.ready } onPress={ () => this.laporan() }/>
                        </View>
                    </View>
                </SwipeablePanel>
                <ModalNotice isVisible={ this.state.afterUpload } message={ this.state.message } status={ this.state.status } onClose={() => this.setState({ afterUpload: false })}/>
            </View>
        );
    }
}

export default VoicePanic;