import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Icon, Text, Row, Col, Thumbnail, Card, CardItem } from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { channel, getFiveVideos } from '../services/vendor/youtube'
import { subTitle } from '../helpers/string'
import CardMenu from '../component/CardMenu'
import LoadingListYt from '../component/LoadingListYt'
import moment from 'moment'
import localization from 'moment/locale/id'
import { updateFcmToken } from '../services/vendor/app'
import { refreshtoken } from '../helpers/refreshtoken'

class Dashboard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      loadingList: true,
      ytList: [],
      channelName: '',
      channelThumbnail: '',
      token: ''
    };
  }

  componentDidMount() {
    refreshtoken()
    this.getToken()
    this.loadVideos()
    this.loadChennel()
    moment().locale('id', localization)
  }

  loadVideos() {
    getFiveVideos().then((result) => {
      this.setState({
        ytList: result
      });
    }).catch(error => {
      console.log(error.response)
    })
  }

  loadChennel() {
    channel().then((result) => {
      this.setState({
        loadingList: false,
        channelName: result.title,
        channelThumbnail: result.thumbnails.medium.url
      })
    }).catch((error) => {
      console.log(error)
    })
  }

  async getToken() {
    try {
      const token = await AsyncStorage.getItem("token")
      this.setState({ token: token })
      this.fcmToken(token)
    } catch (error) {
    
    }
  }

  async fcmToken(token) {
    let fcmToken = await AsyncStorage.getItem('fcmToken')
    
    updateFcmToken(token, fcmToken).then(result => {
      console.log(result)
    }).catch(error => {
      console.log(error)
    })
  }

  redirecTo = async (to) => {
    let token = null
    token = await AsyncStorage.getItem("token")

    if (token !== null) {

      switch (to) {
        case 'KRIMINAL':
          return this.props.navigation.navigate('Report', { type: 'Kriminal', code: '5fa42a852eb8451500392985' });
          break;

        case 'BENCANA':
          return this.props.navigation.navigate('Report', { type: 'Bencana', code: '5fa42a792eb8451500392984' });
          break;

        case 'KESEHATAN':
          return this.props.navigation.navigate('Report', { type: 'Kesehatan', code: '5fa42a642eb8451500392981' });
          break;

        case 'SOSIAL':
          return this.props.navigation.navigate('Report', { type: 'Sosial', code: '5fa42a4a2eb8451500392980' });
          break;

        default:
          break;
      }
    } else {
      return this.props.navigation.navigate("Login")
    }
  }

  render() {
    return (
      <Container style={{ backgroundColor: '#f0f0f0' }}>
        <Header style={{ backgroundColor: '#fff', borderBottomColor: '#f1f1f1', padding: 10, borderBottomWidth: 1 }}>
          <Row>
            <Col style={{ width: '90%' }}>
              <Text style={{ fontSize: 10 }}>Prototipe</Text>
              <Text style={{ fontSize: 20 }}><Text style={{ fontWeight: "bold", fontSize: 20 }}>Hallo</Text> Babinsa</Text>
            </Col>
            <Col style={{ justifyContent: 'center' }}>
              <Icon type="FontAwesome" name="bell-o" style={{ fontSize: 25 }} />
            </Col>
          </Row>
        </Header>
        <Content>
          <Row style={{ padding: 15, backgroundColor: 'white', marginBottom: 15, borderBottomStartRadius: 20, borderBottomEndRadius: 20 }}>
            <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity onPress={() => { this.redirecTo('KRIMINAL') }}>
                <CardMenu title="KRIMINAL">
                  <Thumbnail small source={require('../img/criminal.png')} style={{ borderRadius: 0 }} />
                </CardMenu>
              </TouchableOpacity>
            </Col>
            <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity onPress={() => { this.redirecTo('BENCANA') }}>
                <CardMenu title="BENCANA">
                  <Thumbnail small source={require('../img/fire.png')} style={{ borderRadius: 0 }} />
                </CardMenu>
              </TouchableOpacity>
            </Col>
            <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity onPress={() => { this.redirecTo('KESEHATAN') }}>
                <CardMenu title="KESEHATAN">
                  <Thumbnail small source={require('../img/health.png')} style={{ borderRadius: 0 }} />
                </CardMenu>
              </TouchableOpacity>
            </Col>
            <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity onPress={() => { this.redirecTo('SOSIAL') }}>
                <CardMenu title="SOSIAL">
                  <Thumbnail small source={require('../img/social.png')} style={{ borderRadius: 0 }} />
                </CardMenu>
              </TouchableOpacity>
            </Col>
          </Row>

          <LoadingListYt visible={this.state.loadingList} />
          {
            this.state.ytList.map((data) => {
              return (
                <TouchableOpacity onPress={() => { this.props.navigation.navigate('YTWV', { id: data.id.videoId, identity: data.snippet }) }} key={data.etag} navigation={this.props.navigation}>
                  <Card>
                    <CardItem style={{ paddingLeft: 0, paddingRight: 0, paddingBottom: 0, paddingTop: 0 }}>
                      <Image style={{ resizeMode: 'cover', width: '100%', height: 200 }} source={{ uri: data.snippet.thumbnails.high.url }} />
                    </CardItem>

                    <CardItem style={{ paddingLeft: 10, paddingRight: 10, paddingBottom: 10, paddingTop: 10 }}>
                      <Thumbnail source={{ uri: this.state.channelThumbnail }} style={{ marginRight: 10 }} />
                      <Col>
                        <Text style={{ fontSize: 14 }}>{subTitle(data.snippet.title)}</Text>
                        <Text style={{ marginTop: 5 }} note>{this.state.channelName} • {moment(data.snippet.publishedAt).fromNow()}</Text>
                      </Col>
                    </CardItem>
                  </Card>
                </TouchableOpacity>
              )
            })
          }
        </Content>
      </Container>
    );
  }
}

export default Dashboard;