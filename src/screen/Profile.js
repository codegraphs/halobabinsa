import React, { useEffect, useState } from 'react';
import { View, Alert, Image, TouchableOpacity } from 'react-native'
import Loading from '../component/Loading'
import { Thumbnail, Text, Container, Col, Row, Content, H3, List, ListItem, Button, Icon, Card, CardItem, Header, Badge } from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { getProfile } from '../services/vendor/app'
import { AuthContext } from '../navigation/context'

import moment from 'moment'
import localization from 'moment/locale/id'

const Profile = ({ navigation }) => {

    const signOut = React.useContext(AuthContext);

    const [ fullname, setFullname] = useState("")
    const [ phone, setPhone] = useState("")
    const [ data, setData ] = useState(null)
    const [isLoading, setIsLoading ] = useState(true)
    const [reportPhoto, setReportPhoto ] = useState("")
    const [reportType, setReportType ] = useState("")
    const [reportDescription, setReportDescription ] = useState("")
    const [reportStatus, setReportStatus ] = useState("")
    const [reportDate, setReportDate ] = useState("")
    
    useEffect(() => {
        getMyStringValue()
    })

    const getMyStringValue = async () => {
        try {
            const fullname = await AsyncStorage.getItem('fullname')
            const token = await AsyncStorage.getItem('token').then(result => {
                profile(result)
            }).catch(error => {
                Alert.alert("Gagal Mengambil data")
            })

        } catch(e) {
          console.log(e)
        }  
    }

    const profile = (token) => {
        getProfile(token).then(result => {
            setFullname(result.profile.first_name + ' ' + result.profile.last_name)
            setPhone(result.profile.phone)
            setReportPhoto((result.report == null) ? '' : result.report.attachment.filename)
            setReportType((result.report == null) ? '' : result.report.report_type.name)
            setReportDescription((result.report == null) ? '' : result.report.description)
            setReportStatus((result.report == null) ? '' : result.report.report_status)
            setReportDate((result.report == null) ? '' : result.report.created_at)
            setIsLoading(false)
        }).catch(error => {
            console.log(error)
        })
    }

    return(
        <Container style={{ backgroundColor: '#f0f0f0'}}>
                <Loading visible={ isLoading } />
                <Header style={{ backgroundColor: '#fff', height: 45, borderBottomColor: '#f1f1f1', padding: 10, borderBottomWidth: 1 }}>
                    <Row>
                        <Col></Col>
                        <Col style={{ alignItems: 'center'}}>
                            <Text style={{ fontWeight: 'bold', color: '#777'}}>Akun Saya</Text>
                        </Col>
                        <Col></Col>
                    </Row>
                </Header>
                <Content>
                    <View style={{ backgroundColor: 'white', paddingHorizontal: 15, paddingBottom: 20, borderBottomStartRadius: 20, borderBottomEndRadius: 20 }}>    
                        <Row style={{ marginTop: 15 }}>
                            <Col style={{ width: 80}}>
                                <Thumbnail source={require('../img/account.png')} />
                            </Col>
                            <Col style={{ justifyContent: 'center'}}>
                                <View>
                                    <H3>{ fullname }</H3>
                                    <Text note>{ phone }</Text>
                                </View>
                            </Col>
                        </Row>
                    </View>     

                    {
                        reportPhoto !== '' ? (
                            <View style={{ backgroundColor: 'white', marginTop: 15, paddingTop: 5, paddingBottom: 5 }}>
                            <View style={{ flexDirection: 'row' , padding: 15}}>
                                <View style={{ width: '80%'}}>
                                    <Text>LAPORAN TERAKHIR ANDA</Text>
                                </View>
                                <View style={{ alignItems: 'flex-end'}}>
                                    <Badge danger>
                                        <Text style={{ fontSize: 12, fontWeight: 'bold' }}>{ reportStatus }</Text>
                                    </Badge>
                                </View>
                            </View>
                            <Card>
                                <CardItem style={{ paddingLeft: 0, paddingRight: 0, paddingBottom: 0, paddingTop: 0 }}>
                                <Image style={{ resizeMode: 'cover', width: '100%', height: 200 }} source={{ uri: 'http://162.0.224.91:3001/uploads/' + reportPhoto }} />
                                </CardItem>
    
                                <CardItem style={{ paddingLeft: 0, paddingRight: 0, paddingBottom: 0, paddingTop: 0 }}>
                                <Col style={{ backgroundColor: 'red', alignItems: 'center'}}>
                                    <Text style={{ fontWeight:'500', color: 'white', margin: 5 }}>{ reportType }</Text>
                                </Col>
                                </CardItem>
    
                                <CardItem>
                                <Col>
                                    <Text>{ reportDescription }</Text>
                                    <Text style={{marginTop: 15}} note>{ moment(reportDate).fromNow() }</Text>
                                </Col>
                                </CardItem>
                            </Card>
                        </View>
                        ) : (
                            <View style={{ alignItems: 'center', justifyContent: 'center', marginVertical: 15, backgroundColor: '#fff', paddingVertical: 15}}>
                                <Text style={{ fontWeight: 'bold' }}>Anda Belum Melapor Apapun</Text>
                            </View>
                        )
                    }

                    <View style={{ backgroundColor: 'white', marginTop: 15 }}>
                        <List>
                            <ListItem>
                                <TouchableOpacity style={{ flexDirection: 'row'}} onPress={ () => this.props.navigation.navigate("EditProfile")}>
                                    <View style={{ width: 45, alignItems: 'center' }}>
                                        <Icon type="FontAwesome" name="vcard-o" style={{ fontSize: 20 }}/>
                                    </View>
                                    <View>
                                        <Text>Ubah Data Pribadi</Text>
                                    </View>
                                </TouchableOpacity>
                            </ListItem>
                            <ListItem>
                                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={ () => this.props.navigation.navigate("UpdatePassword")}>
                                    <View style={{ width: 45, alignItems: 'center' }}>
                                        <Icon type="FontAwesome" name="key" style={{ fontSize: 20 }}/>
                                    </View>
                                    <View>
                                        <Text>Ubah Kata Sandi</Text>
                                    </View>
                                </TouchableOpacity>
                            </ListItem>
                            <ListItem>
                                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={ () => this.props.navigation.navigate("Eula")}>
                                    <View style={{ width: 45, alignItems: 'center' }}>
                                        <Icon type="FontAwesome" name="balance-scale" style={{ fontSize: 20 }}/>
                                    </View>
                                    <View>
                                        <Text>Syarat dan Ketentuan</Text>
                                    </View>
                                </TouchableOpacity>
                            </ListItem>
                            <ListItem>
                                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={ () => this.props.navigation.navigate("About") }>
                                    <View style={{ width: 45, alignItems: 'center' }}>
                                        <Icon type="FontAwesome" name="copyright" style={{ fontSize: 20 }}/>
                                    </View>
                                    <View>
                                        <Text>Tentang Halo Babinsa</Text>
                                    </View>
                                </TouchableOpacity>
                            </ListItem>
                        </List>
                        <View style={{ marginTop: 15, padding: 25 }} onPress={ () => signOut() }>
                            <Button block danger >
                                <Text>Keluar</Text>
                            </Button>
                        </View>
                    </View>
                    
                    <View style={{ alignItems: 'center', marginTop: 25, marginBottom: 25 }}>
                        <Text style={{ fontWeight: 'bold', color: '#777', fontSize: 20, marginBottom: 15}}>PUSTERAD x ADITARA</Text>
                        <Text style={{ fontWeight: 'bold', color: '#888'}}>PUSAT TERITORIAL ANGKATAN DARAT</Text>  
                        <Text style={{ fontWeight: 'bold', color: '#888'}}>PT. Aplikasi Digital Nusantara</Text>      
                    </View>
                </Content>
            </Container>
    )
}

export default Profile;