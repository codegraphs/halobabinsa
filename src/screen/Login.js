import React from 'react';
import { View, Text, Alert, Image } from 'react-native';
import { Button, Card } from 'native-base';
import Input from '../component/Input'
import { login } from '../services/vendor/app'
import Loading from '../component/Loading'
import { AuthContext } from '../navigation/context'


const Login = ({ navigation }) => {
    const [ data, setData ] = React.useState({
        username: '',
        password: '',
        check_textInputChange: false,
        secureTextEntry: true,
        isValidUser: true,
        isValidPassword: true,
        isLoading: false
    })

    const { signIn } = React.useContext(AuthContext);

    const textInputChange = (val) => {
        if( val.trim().length >= 4 ) {
            setData({
                ...data,
                username: val,
                check_textInputChange: true,
                isValidUser: true
            });
        } else {
            setData({
                ...data,
                username: val,
                check_textInputChange: false,
                isValidUser: false
            });
        }
    }

    const handlePasswordChange = (val) => {
        if( val.trim().length >= 8 ) {
            setData({
                ...data,
                password: val,
                isValidPassword: true
            });
        } else {
            setData({
                ...data,
                password: val,
                isValidPassword: false
            });
        }
    }

    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }

    const loginHandle = (username, password) => {
        setData({
            ...data,
            isLoading: true
        })

        if ( username.length == 0 || password.length == 0 ) {
            setData({
                ...data,
                isLoading: false
            })
            Alert.alert('Kesalahan!', 'Email atau password tidak boleh kosong.', [
                {text: 'Okay'}
            ]);
            return;
        }

        login(username, password).then((result) => {
            setData({
                ...data,
                isLoading: false
            })

            signIn(result._id, result.access_token)
        }).catch((error) => {
            setData({
            ...data,
            isLoading: false
        })
            Alert.alert('Kesalahan!', 'Email atau Password Salah')
        })
    }

    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Loading visible={ data.isLoading }/>
            <View style={{ width: '90%' }}>
                <Card>
                    <View style={{ paddingTop: 30 }}>
                        <View style={{ alignItems: 'center' }}>
                            <Image source={require('../img/HaloBabinsa.png')} style={{ height: 100, width: 200, resizeMode: 'stretch'}}/>
                            <Text>PROTOTYPE</Text>
                        </View>
                        <View style={{ paddingHorizontal: 20, marginTop: 25 }}>
                            <Input title="E-Mail" placeholder="E-Mail" onChangeText={text => textInputChange(text)} autoCompleteType='email' keyboardType='email-address' autoCapitalize='none'/>
                            <Input title="Password" placeholder="Password" onChangeText={text => handlePasswordChange(text)} secureTextEntry={true} />
                        </View>
                    </View>
                    <Button block success onPress={ () => { loginHandle(data.username, data.password) } } style={{ borderRadius: 0}}>
                        <Text style={{ color: '#fff' }}>MASUK</Text>
                    </Button>
                    <Button block light onPress={ () => { navigation.navigate("Register") } } style={{ borderRadius: 0}}>
                        <Text style={{ color: '#fff' }}>DAFTAR</Text>
                    </Button>
                </Card>
            </View>
        </View>
    );
}

export default Login;