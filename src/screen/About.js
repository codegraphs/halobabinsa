import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { Icon } from 'native-base'

class About extends React.Component{
    render(){
        return(
            <View style={{ flex: 1 }}>
                <View style={{ height: 45, backgroundColor: '#fff', flexDirection: 'row'}}>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={ () => this.props.navigation.goBack() }>
                            <Icon type="Feather" name="arrow-left" style={{ color: "#777", fontWeight: 300}}/>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '80%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{ fontWeight: 'bold', color: '#777'}}>Tentang Aplikasi</Text>
                    </View>
                    <View style={{ width: '10%'}}> 
                    </View>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', padding: 20, backgroundColor: '#fff'}}>
                    <Image source={require('../img/HaloBabinsa.png')} style={{ marginBottom: 25, resizeMode: 'stretch', height: 100, width: 190 }}/>
                    <Text style={{ fontWeight: 'bold', fontSize: 25 }}>Prototype</Text>
                    <Text style={{ fontWeight: 'bold' }}>Versi 0.5</Text>
                    <Text style={{ textAlign: 'center', marginVertical: 20}}>HaloBabinsa merupakan Aplikasi Prototipe yang dikembangkan oleh Direktorat Penelitian dan Pengembangan PUSAT TERITORIAL ANGKATAN DARAT dengan pemuda pemudi bangsa (PT. Aplikasi Digital Nusantara) yang ditujukan untuk mempermudah masyarakat yang membutuhkan bantuan BABINSA.</Text>
                </View>
            </View>
        );
    }
}
export default About