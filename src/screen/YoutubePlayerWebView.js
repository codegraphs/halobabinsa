import React, { Component } from 'react'
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native'
import { Icon } from 'native-base'
import { WebView } from 'react-native-webview'
import moment from 'moment'

const YoutubePlayerWebView = ({ route, navigation, identity }) => {
    console.log('https://www.youtube.com/embed/' + route.params?.id)
    return(
        <View style={{ flex: 1 }}>
            <View style={{ height: 45, backgroundColor: '#fff', flexDirection: 'row'}}>
                <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={ () => navigation.navigate('Main') }>
                        <Icon type="Feather" name="arrow-left" style={{ color: "#777", fontWeight: 300}}/>
                    </TouchableOpacity>
                </View>
                <View style={{ width: '80%', alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{ fontWeight: 'bold', color: '#777'}}>Video TNI AD</Text>
                </View>
                <View style={{ width: '10%'}}>

                </View>
            </View>
            <View style={{ height: 200 }}>
                <WebView allowsFullscreenVideo
                    allowsInlineMediaPlayback
                    mediaPlaybackRequiresUserAction
                    source={{ uri:'https://www.youtube.com/embed/' + route.params?.id }} />
            </View>
            <View style={{ paddingHorizontal: 10, paddingVertical: 15, borderBottomColor: '#dbdbdb', borderBottomWidth: 0.5 }}>
                <Text style={{ fontWeight: '500', fontSize: 14 }}>{ route.params?.identity.title }</Text>
                <Text style={{ color: '#999', fontSize: 11, fontWeight: '500', paddingVertical: 5 }}>{ moment(route.params?.identity.publishTime).fromNow() }</Text>
            </View>
        </View>
    )
}

export default YoutubePlayerWebView;