import React from 'react'
import { View, Text, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import { Icon } from 'native-base'
import { RNCamera } from 'react-native-camera';
import { SwipeablePanel } from 'rn-swipeable-panel';
import { TextInput } from 'react-native-gesture-handler';
import GetLocation from 'react-native-get-location'
import Geocoder from 'react-native-geocoding';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Modal from 'react-native-modal'
import ModalNotice from '../component/ModalNotice'
import Buttons from '../component/Button'
import axios from 'axios'

class Report extends React.Component {
    constructor(){
    	super()
        this.state = {
            flash: false,
            camera: 1,
            flashMode: RNCamera.Constants.FlashMode.off,
            cameraMode: RNCamera.Constants.Type.back,
            afterCapture: false,
            image: '',
            ratio: '4:3',
            latitude: null,
            longitude: null,
            address: '',
            description: '',
            ready: true,
            token: '',
            onUpload: false,
            message: '',
            afterUpload: false,
            status: ''
    	}
    }

    componentDidMount() {
        this.position()
        this.getToken()
    }

    async getToken() {
        try {
            const token = await AsyncStorage.getItem("token")
            this.setState({ token: token})
        } catch (error) {
            
        }
    }

    laporan() {
        this.setState({ onUpload: true })
        var formData = new FormData();

        formData.append('report_type', this.props.route.params.code);
        formData.append('address', this.state.address);
        formData.append('latitude', this.state.latitude);
        formData.append('longitude', this.state.longitude);
        formData.append('description', (this.state.description == '') ? 'Tanpa Deskripsi' : this.state.description);
        formData.append('attachment', {
            uri: this.state.image,
            type: 'image/jpeg',
            name: 'laporan.jpeg'
        });

        axios.post('http://162.0.224.91:3001/api/v1/report/',
            formData, {
                headers: {
                    'Content-Type': 'multipart/form-data; boundary=<calculated when request is sent>',
                    'x-access-token': `${this.state.token}`
                }
        }).then(response => {
            console.log(response.data)
            this.setState({ onUpload: false, afterCapture: false, afterUpload: true, message: "Berhasil Melapor Ke Babinsa", status: "success" })
        }).catch(error => {
            console.log(error.response)
            console.log(formData)
            this.setState({ onUpload: false, afterCapture: false, afterUpload: true, message: "Gagal Melapor Ke Babinsa", status: "fail" })
        })
    }

    position() {
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        })
        .then(location => {
            this.setState({
                latitude: location.latitude,
                longitude: location.longitude
            });

            Geocoder.init('AIzaSyCK6mtOzzeFG4W7o1k7B0XuOJPxWjZaB2E');
            Geocoder.from({
                latitude: location.latitude,
                longitude: location.longitude
            }).then(result =>  {
                this.setState({
                    address: result.results[0].formatted_address,
                    ready: false
                })
            })
        })
        .catch(error => {
            const { code, message } = error;
            console.warn(code, message);
        })
    }

    changeFlash(mode) {
        if (mode == false) {
            this.setState({ flash: true, flashMode: RNCamera.Constants.FlashMode.on })
        } else {
            this.setState({ flash: false, flashMode: RNCamera.Constants.FlashMode.off })
        }
    }

    changeCamera(mode) {
        if (mode == 1) {
            this.setState({ camera: 2, flashMode: RNCamera.Constants.Type.front })
        } else {
            this.setState({ camera: 1, flashMode: RNCamera.Constants.Type.back })
        }
    }

    flashMode() {
        if (this.state.flash == false) {
            return <Icon type="Feather" name="zap-off" style={{ color: "#333", fontWeight: 300, fontSize: 25 }}/>
        } else {
            return <Icon type="Feather" name="zap" style={{ color: "#333", fontWeight: 300, fontSize: 25 }}/>
        }
    }
    
    takePicture = async () => {
        if (this.camera) {
            const options = { 
                quality: 0.5, 
                base64: true 
            };
            const data = await this.camera.takePictureAsync(options);
            this.setState({ afterCapture: true, image: data.uri });
            console.log(data.uri)
        }
    }

    prepareRatio = async () => {
        if (Platform.OS === 'android' && this.cam) {
            const ratios = await this.cam.getSupportedRatiosAsync();

            const ratio = ratios.find((ratio) => ratio === "4:3") || ratios[ratios.length - 1];
             
            this.setState({ ratio });
        }
    }

    render() {
        return(
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={{ height: '7%', flexDirection: 'row', backgroundColor: '#f1f1f1' }}>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center'}}>
                        <TouchableOpacity onPress={ () => this.props.navigation.navigate('Main') }>
                            <Icon type="Feather" name="arrow-left" style={{ color: "#333", fontWeight: 300}}/>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '80%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Icon type="Feather" name="map-pin" style={{ fontSize: 12, marginRight: 5 }}/><Text style={{ fontSize: 12 }}>{ this.state.address.substring(0, 35) }</Text>
                    </View>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center'}}>
                        <TouchableOpacity onPress={ () => this.changeFlash(this.state.flash) }>
                            { this.flashMode() }
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ height: '80%' }}>
                    <RNCamera
                        ref={ref => {
                            this.camera = ref;
                        }}
                        onCameraReady={this.prepareRatio}
                        style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}
                        type={ (this.state.camera == 1) ?  RNCamera.Constants.Type.back : RNCamera.Constants.Type.front }
                        flashMode={ this.state.flashMode }
                        ratio={this.state.ratio}
                        androidCameraPermissionOptions={{
                            title: 'Permission to use camera',
                            message: 'We need your permission to use your camera',
                            buttonPositive: 'Ok',
                            buttonNegative: 'Cancel',
                        }}
                        androidRecordAudioPermissionOptions={{
                            title: 'Permission to use audio recording',
                            message: 'We need your permission to use your audio',
                            buttonPositive: 'Ok',
                            buttonNegative: 'Cancel',
                        }}
                    />
                </View>
                <View style={{ height: '13%', flexDirection: 'row', backgroundColor: '#f1f1f1' }}>
                    <View style={{ width: '33%', alignItems: 'center', justifyContent: 'center'}}>
                        
                    </View>
                    <View style={{ width: '33%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <TouchableOpacity onPress={this.takePicture.bind(this)}>
                            <Icon type="FontAwesome" name="camera" style={{ fontSize: 35 }}/>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '33%', alignItems: 'center', justifyContent: 'center'}}>
                        <TouchableOpacity onPress={() => this.changeCamera(this.state.camera)}>
                            <Icon type="Ionicons" name="camera-reverse-outline" style={{ color: '#333' }}/>
                        </TouchableOpacity>
                    </View>
                </View>
                <SwipeablePanel isActive={this.state.afterCapture} fullWidth={true} openLarge={true} onClose={ () => this.setState({ afterCapture: false }) }>
                    <View style={{ flex: 1, padding: 10 }}>
                        <View style={{ marginVertical: 10, alignItems: "center"}}>
                            <Text style={{ fontSize: 15, fontWeight: 'bold'}}>Pelaporan Tentang { this.props.route.params.type }</Text>
                        </View>
                        <View>
                            <Image source={{ uri: `${this.state.image}` }} style={{ minWidth: 200, minHeight: 400 }}/>
                        </View>
                        <View style={{ marginVertical: 10}}>
                            <Text style={{ fontWeight: 'bold'}}>Deskripsi Masalah</Text>
                            <TextInput style={{ borderColor: '#f1f1f1', borderWidth: 1, marginVertical: 5, borderRadius: 15}} onChangeText={ text => this.setState({ description: text }) }/>
                        </View>
                        <View style={{ marginVertical: 10}}>
                            <Text style={{ fontWeight: 'bold'}}>Lokasi Masalah</Text>
                            <View style={{ flexDirection: 'row', marginBottom: 15, alignItems: 'center' }}>
                                <Icon type="Feather" name="map-pin" style={{ fontSize: 12, marginRight: 5 }}/><Text style={{ fontSize: 12 }}>{ this.state.address.substring(0, 35) }</Text>
                            </View>
                        </View>
                        <View style={{ justifyContent: 'flex-end', flex:1, alignItems: 'center'}}>
                            <Buttons title="Kirim" disabled={ this.state.ready } onPress={ () => this.laporan() }/>
                        </View>
                    </View>
                </SwipeablePanel>
                <Modal isVisible={ this.state.onUpload } style={{ alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{ backgroundColor: '#fff', height: 150, width: 150, alignItems: 'center', justifyContent: 'center', borderRadius: 15 }}>
                        <ActivityIndicator size="large" color="#333" style={{ marginBottom: 30 }}/>
                        <Text>Sedang Mengirim</Text>
                    </View>
                </Modal>
                <ModalNotice isVisible={ this.state.afterUpload } message={ this.state.message } status={ this.state.status } onClose={() => this.setState({ afterUpload: false })}/>
            </View>
        )
    }
}

export default Report