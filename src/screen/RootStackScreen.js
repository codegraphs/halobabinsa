import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import Login from './Login';
import Register from './Register';
import Main from './Main';
import YoutubePlayerWebView from './YoutubePlayerWebView'

const RootStack = createStackNavigator();

const RootStackScreen = ({navigation}) => (
    <RootStack.Navigator headerMode='none'>
        <RootStack.Screen name="Main" component={Main}/>
        <RootStack.Screen name="YTWV" component={YoutubePlayerWebView} />
        <RootStack.Screen name="Login" component={Login}/>
        <RootStack.Screen name="Register" component={Register}/>
    </RootStack.Navigator>
);

export default RootStackScreen;