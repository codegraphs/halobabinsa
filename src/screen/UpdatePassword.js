import React from 'react'
import { View, Text, TouchableOpacity, Alert } from 'react-native'
import { Icon, Button } from 'native-base'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loading from '../component/Loading'
import Input from '../component/Input'
import { AuthContext } from '../navigation/context'
import { updatePassword } from '../services/vendor/app'

class UpdatePassword extends React.Component {
    static contextType = AuthContext;

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            password: '',
            passwordConfirm: '',
            token: ''
        }
    }

    componentDidMount() {
        this.getToken()
    }

    getToken = async () => {
        try {
            const token = await AsyncStorage.getItem('token').then(result => {
                this.setState({ token: result })
            }).catch(error => {
                Alert.alert("Gagal Mengambil data")
            })
        } catch(e) {
          console.log(e)
        }  
    }

    updatePassword = async () => {
        if (this.state.password == '' || this.state.passwordConfirm == '') {
            Alert.alert("Peringatan!", "Isian tidak boleh kosong")
        } else if(this.state.password !== this.state.passwordConfirm) {
            Alert.alert("Peringatan!", "Isian harus memiliki isi sama")
        } else if(this.state.password.length <= 6) {
            Alert.alert("Peringatan!", "Password Harus Lebih dari 6 digit")
        } else {
            const password = {
                password: this.state.password
            }

            updatePassword(this.state.token, password).then( result => {
                const { signOut } = this.context
                
                Alert.alert("Berhasil", "Silahkan masuk kembali", [
                    { 
                        text: "OK", 
                        onPress: () => { signOut() } 
                    }], { 
                    cancelable: false 
                })
            }).catch( error => {
                console.log(error)
            })
        }
    }


    render() {
        return(
            <View style={{ flex: 1 }}>
                <Loading visible={ this.state.isLoading }/>
                <View style={{ height: 45, backgroundColor: '#fff', flexDirection: 'row'}}>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={ () => this.props.navigation.goBack() }>
                            <Icon type="Feather" name="arrow-left" style={{ color: "#777", fontWeight: 300}}/>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '80%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{ fontWeight: 'bold', color: '#777'}}>Ubah Password</Text>
                    </View>
                    <View style={{ width: '10%'}}> 
                    </View>
                </View>
                <View style={{ backgroundColor: '#fff', padding: 10 }}>
                    <Input title="Password Baru" placeholder="Password Baru" onChangeText={text => this.setState({ password: text})} secureTextEntry={true}/>
                    <Input title="Konfirmasi Password Baru" placeholder="Konfirmasi Password Baru" onChangeText={text => this.setState({ passwordConfirm: text})} secureTextEntry={true}/>
                </View>
                <Button success block onPress={ () => this.updatePassword() }>
                    <Text style={{ color: '#fff' }}>Simpan</Text>
                </Button>
            </View>
        )
    }
}

export default UpdatePassword