import React, { useEffect } from 'react';
import { View, Text } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import Logo from '../img/logo.svg'
import AsyncStorage from '@react-native-async-storage/async-storage';

const Splash = ({
    navigation
}) => {
    useEffect(() =>{
        setTimeout(() => {
            const auth = AsyncStorage.getItem("token").then( res => {
                if (res != null) {
                    navigation.navigate("Main")
                } else {
                    navigation.navigate("Login")   
                }
            })
        }, 3000);
    })

    return(
        <View style={{ flex: 1 }}>
            <LinearGradient style={{ flex: 1, height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'space-between' }} colors={[ '#67B144', '#1D8D40', '#007F3E' ]}>
                <View></View>
                <View style={{ alignItems: 'center' }}>
                    <Logo width={250} height={250}/>
                </View>
                <View style={{ alignItems: 'center', padding: 20 }}>
                    <Text style={{ color: '#fff' }}>Dipersembahkan oleh.</Text>
                    <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 15 }}>PUSTERAD</Text>
                    <Text style={{ color: '#fff' }}>2020</Text>
                </View>
            </LinearGradient>
        </View>
    );
}

export default Splash;