import React, { Component } from 'react';
import { Container, Footer, FooterTab, Button, Icon, Text } from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Dashboard from './Dashboard'
import Profile from './Profile'

export default class Main extends Component {
    constructor() {
        super()
        this.state = {
            selectedTab: '',
        }
    }

    page() {
        switch (this.state.selectedTab) {
            case "Dashboard":
                return (<Dashboard navigation={this.props.navigation} />);
                break;

            case "Profile":
                return (<Profile navigation={this.props.navigation} />);
                break;

            default:
                return (<Dashboard navigation={this.props.navigation} />);
                break;
        }
    }

    redirectTo = async (to) => {
        let token = null
        token = await AsyncStorage.getItem("token")

        if (token !== null) {
            switch (to) {
                case 'PROFILE':
                    return this.setState({ selectedTab: 'Profile' })
                    break;

                case 'VOICE':
                    return this.props.navigation.navigate("VoicePanic")
                    break;

                case 'CAMERA':
                    return this.props.navigation.navigate("CameraPanic")
                    break;

                default:
                    break;
            }
        } else {
            return this.props.navigation.navigate("Login")
        }
    }


    render() {
        return (
            <Container>
                {
                    this.page()
                }
                <Footer>
                    <FooterTab style={{ backgroundColor: '#f1f1f1' }}>
                        <Button vertical onPress={() => this.setState({ selectedTab: 'Dashboard' })} style={{ width: '20%' }}>
                            <Icon type="FontAwesome" style={{ color: 'grey' }} name="home" />
                            <Text style={{ color: 'grey' }}>Beranda</Text>
                        </Button>
                        <Button vertical onPress={() => this.redirectTo('CAMERA')} style={{ backgroundColor: 'grey', borderBottomLeftRadius: 15, borderTopLeftRadius: 15, borderBottomRightRadius: 0, borderTopRightRadius: 0 }}>
                            <Icon type="FontAwesome" style={{ color: 'white' }} name="camera" />
                        </Button>
                        <Button vertical onPress={() => this.redirectTo('VOICE') } style={{ backgroundColor: 'grey', borderBottomLeftRadius: 0, borderTopLeftRadius: 0, borderBottomRightRadius: 15, borderTopRightRadius: 15 }}>
                            <Icon type="FontAwesome" style={{ color: 'white' }} name="microphone" />
                        </Button>
                        <Button vertical onPress={() => this.redirectTo('PROFILE')} style={{ width: '20%' }}>
                            <Icon type="FontAwesome" style={{ color: 'grey' }} name="user" />
                            <Text style={{ color: 'grey' }} >Profil</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}