import React from 'react'
import { View, Text, TouchableOpacity, ScrollView, Alert } from 'react-native'
import { Icon, Button } from 'native-base'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Input from '../component/Input'
import { getProfile, updateProfile } from '../services/vendor/app'
import Loading from '../component/Loading'

class EditProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            first_name: '',
            last_name: '',
            email: '',
            phone: '',
            token: ''
        }
    }

    componentDidMount() {
        this.getMyStringValue()
    }

    getMyStringValue = async () => {
        try {
            const fullname = await AsyncStorage.getItem('fullname')
            const token = await AsyncStorage.getItem('token').then(result => {
                this.setState({ token: result})
                this.profile(result)
            }).catch(error => {
                Alert.alert("Gagal Mengambil data")
            })

        } catch(e) {
          console.log(e)
        }  
    }

    profile(token) {
        getProfile(token).then(result => {
            this.setState({ 
                first_name: result.profile.first_name,
                last_name: result.profile.last_name,
                email: result.profile.email, 
                phone: result.profile.phone,
                isLoading: false
            })
        }).catch(error => {
            console.log(error)
        })
    }

    updateProfile = async () => {
        this.setState({ isLoading: true })
        const data = {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            phone: this.state.phone,
            email: this.state.email,
        }

        updateProfile(this.state.token, data).then( response => {
            this.setState({ isLoading: false })
            Alert.alert("Berhasil", "Data anda berhasil dirubah", [
                { text: "OK", onPress: () => this.props.navigation.navigate("Profile") }
              ],
              { cancelable: false })
        }).catch( error => {
            this.setState({ isLoading: false })
            Alert.alert("Gagal", "Data anda gagal dirubah")
        })

    }

    render() {
        return(
            <View style={{ flex: 1 }}>
                <Loading visible={ this.state.isLoading }/>
                <View style={{ height: 45, backgroundColor: '#fff', flexDirection: 'row'}}>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={ () => this.props.navigation.navigate('Main') }>
                            <Icon type="Feather" name="arrow-left" style={{ color: "#777", fontWeight: 300}}/>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '80%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{ fontWeight: 'bold', color: '#777'}}>Data Pribadi</Text>
                    </View>
                    <View style={{ width: '10%'}}>

                    </View>
                </View>
                <ScrollView>
                    <View style={{ backgroundColor: '#fff', padding: 10}}>
                        <Input title="Nama Depan" placeholder="Nama Depan" value={ this.state.first_name } onChangeText={text => this.setState({ first_name: text})}/>
                        <Input title="Nama Belakang" placeholder="Nama Belakang" value={ this.state.last_name } onChangeText={text => this.setState({ last_name: text})}/>
                        <Input title="E-Mail" placeholder="E-Mail" value={ this.state.email } onChangeText={text => this.setState({ email: text})}/>
                        <Input title="Nomor Telepon" placeholder="Nomor Telepon" value={ this.state.phone } onChangeText={text => this.setState({ phone: text})}/>
                    </View>
                    <Button success block onPress={ () => this.updateProfile() }>
                        <Text style={{ color: '#fff' }}>Simpan</Text>
                    </Button>
                </ScrollView>
            </View>
        );
    }
}
export default EditProfile;