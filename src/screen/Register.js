import React from 'react';
import { View, Text, ScrollView, TouchableOpacity, Alert } from 'react-native';
import { Button, Icon } from 'native-base';
import Input from '../component/Input';
import Loading from '../component/Loading';
import { register } from '../services/vendor/app'

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            first_name: '',
            last_name: '',
            email: '',
            phone: '',
            password: '',
            passwordConfirm: ''
        }
    }

    register = async () => {
        this.setState({ isLoading: true })
        if (this.state.first_name.length == 0 || this.state.last_name.length == 0 || this.state.email.length == 0 || this.state.phone.length == 0 || this.state.password.length == 0 || this.state.passwordConfirm == 0 ) {
            this.setState({ isLoading: false })
            Alert.alert("Peringatan", 'Semua kolom harus diisi')
        } else if (this.state.password !== this.state.passwordConfirm) {
            this.setState({ isLoading: false })
            Alert.alert("Peringatan", "Konfirmasi Password tidak sesuai")
        } else {
            register(this.state.first_name, this.state.last_name, this.state.email, this.state.phone, this.state.password).then( result => {
                this.setState({ isLoading: false })
                Alert.alert('Berhasil', 'anda berhasil terdaftar', [
                    { text: "OK", onPress: () => this.props.navigation.navigate("Login") }
                  ],
                  { cancelable: false });
            }).catch(error => {
                this.setState({ isLoading: false })
                Alert.alert('Peringatan', error.data.message);
            })
        }
    }

    render() {
        return(
            <View style={{ flex: 1 }}>
                <Loading visible={ this.state.isLoading }/>
                <View style={{ height: 45, backgroundColor: '#fff', flexDirection: 'row'}}>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={ () => this.props.navigation.navigate('Login') }>
                            <Icon type="Feather" name="arrow-left" style={{ color: "#777", fontWeight: 300}}/>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '80%', alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{ fontWeight: 'bold', color: '#777'}}>Daftar</Text>
                    </View>
                    <View style={{ width: '10%'}}>

                    </View>
                </View>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ backgroundColor: '#fff', padding: 10}}>
                        <Input title="Nama Depan" placeholder="Nama Depan" onChangeText={text => this.setState({ first_name: text})}/>
                        <Input title="Nama Belakang" placeholder="Nama Belakang" onChangeText={text => this.setState({ last_name: text})}/>
                        <Input title="E-Mail" placeholder="E-Mail" onChangeText={text => this.setState({ email: text})} keyboardType='email-address' autoCapitalize='none'/>
                        <Input title="Nomor Telepon" placeholder="Nomor Telepon" onChangeText={text => this.setState({ phone: text})} keyboardType='numeric'/>
                        <Input title="Password" placeholder="Password" onChangeText={text => this.setState({ password: text})} secureTextEntry={true}/>
                        <Input title="Konfirmasi Password" placeholder="Konfirmasi Password" onChangeText={text => this.setState({ passwordConfirm: text})} secureTextEntry={true}/>
                    </View>
                    <Button success block onPress={ () => this.register() }>
                        <Text style={{ color: '#fff' }}>DAFTAR</Text>
                    </Button>
                </ScrollView>
            </View>
        );
    }
}
export default Register;