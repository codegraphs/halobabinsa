import 'react-native-gesture-handler';
import * as React from 'react';
import Routes from './src/navigation/Routes'
import { Alert, PermissionsAndroid } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import firebase from "react-native-firebase";
import SplashScreen from 'react-native-splash-screen';

class App extends React.Component {

  async componentDidMount() {
    //we check if user has granted permission to receive push notifications.
    this.checkPermission();
    // Register all listener for notification 
    this.createNotificationListeners();
    this.permission()
    SplashScreen.hide();
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    // If Premission granted proceed towards token fetch
    if (enabled) {
      this.getToken();
    } else {
      // If permission hasn’t been granted to our app, request user in requestPermission method. 
      this.requestPermission();
    }
  }

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
        console.log(fcmToken)
      }
    }
  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  async createNotificationListeners() {

    // This listener triggered when notification has been received in foreground
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const { title, body } = notification;
      this.displayNotification(title, body);
    });

    // This listener triggered when app is in backgound and we click, tapped and opened notifiaction
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const { title, body } = notificationOpen.notification;
      this.displayNotification(title, body);
    });

    // This listener triggered when app is closed and we click,tapped and opened notification 
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      this.displayNotification(title, body);
    }
  }


  displayNotification(title, body) {
    // we display notification in alert box with title and body
    Alert.alert(
      title, body,
      [
        { text: 'Ok', onPress: () => console.log('ok pressed') },
      ],
      { cancelable: false },
    );
  }

  permission = async () => {
    if (Platform.OS === 'android') {
      try {
          const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.CAMERA,
              {
                  title: "Cool Photo App Camera Permission",
                  message:
                    "Cool Photo App needs access to your camera " +
                    "so you can take awesome pictures.",
                  buttonPositive: 'ok',
              },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              console.log('You can use the storage');
          } else {
              console.log('permission denied');
              return;
          }
      } catch (err) {
          console.warn(err);
          return;
      }
    }
    
    if (Platform.OS === 'android') {
      try {
          const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
              {
                  title: 'Permissions for write access',
                  message: 'Give permission to your storage to write a file',
                  buttonPositive: 'ok',
              },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              console.log('You can use the storage');
          } else {
              console.log('permission denied');
              return;
          }
      } catch (err) {
          console.warn(err);
          return;
      }
    }
    
    if (Platform.OS === 'android') {
      try {
          const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
              {
                  title: 'Permissions for write access',
                  message: 'Give permission to your storage to write a file',
                  buttonPositive: 'ok',
              },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              console.log('You can use the camera');
          } else {
              console.log('permission denied');
              return;
          }
      } catch (err) {
          console.warn(err);
          return;
      }
    }
    
    if (Platform.OS === 'android') {
      try {
          const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
              {
                  title: 'Permissions for fine location',
                  message: 'Give permission to your GPS sensor',
                  buttonPositive: 'ok',
              },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              console.log('You can use GPS');
          } else {
              console.log('permission denied');
              return;
          }
      } catch (err) {
          console.warn(err);
          return;
      }
    }
  }

  render() {
    return (
      <Routes/>
    );
  }
}

export default App;